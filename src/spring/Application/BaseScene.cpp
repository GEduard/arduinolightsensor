#include <spring\Application\BaseScene.h>

namespace Spring
{
	BaseScene::BaseScene(const std::string & ac_szSceneName) : IScene(ac_szSceneName)
	{

	}

	void BaseScene::createScene()
	{
		
		std::string windowTitle = boost::any_cast<std::string>(m_TransientDataCollection.find("appNameLEdit")->second);
		refreshRate = boost::any_cast<unsigned>(m_TransientDataCollection.find("refreshRateDSBox")->second);
		sampleRate = boost::any_cast<unsigned>(m_TransientDataCollection.find("sampleRateDSBox")->second);
		displayTime = boost::any_cast<double>(m_TransientDataCollection.find("displayTimeDSBox")->second);

		m_uMainWindow->setWindowTitle(QString::fromStdString(windowTitle));
		createGUI();

		connect(startButton, SIGNAL(clicked()), this, SLOT(mf_StartTimer()));
		connect(stopButton, SIGNAL(clicked()), this, SLOT(mf_CleanPlot()));
		connect(stopButton, SIGNAL(clicked()), this, SLOT(mf_StopTimer()));
		connect(returnButton, SIGNAL(clicked()), this, SLOT(returnClick()));

		freq = 50;
		arduinoAvailable = false;
		for (const auto& portInfo : QSerialPortInfo::availablePorts())
		{
			if (portInfo.hasVendorIdentifier())
				if (portInfo.hasProductIdentifier())
				{
					arduinoAvailable = true;
					portName = portInfo.portName();
					break;
				}	
		}

		if (arduinoAvailable)
		{
			arduinoDevice = new QSerialPort();
			arduinoDevice->setPortName(portName);
			arduinoDevice->setBaudRate(9600);
			arduinoDevice->setDataBits(QSerialPort::Data8);
			arduinoDevice->setFlowControl(QSerialPort::NoFlowControl);
			arduinoDevice->setParity(QSerialPort::NoParity);
			arduinoDevice->setStopBits(QSerialPort::OneStop);
		}

		validValues = "";
		customPlot->addGraph();
		double nrSamples = sampleRate * displayTime;

		for (int i = 0; i < nrSamples; i++)
		{
			xAxis.push_back((double)i * 1.0 / sampleRate);
		}
		yAxis = QVector<double>(xAxis.size());
	}

	void BaseScene::release()
	{
		delete centralWidget;
	}

	void BaseScene::createGUI()
	{
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		centralWidgetLayout = new QGridLayout(centralWidget);
		centralWidgetLayout->setSpacing(5);
		centralWidgetLayout->setContentsMargins(10, 10, 10, 10);
		centralWidgetLayout->setObjectName(QStringLiteral("centralWidgetLayout"));
		gridLayout = new QGridLayout();
		gridLayout->setSpacing(5);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));

		customPlot = new QCustomPlot(centralWidget);
		customPlot->setObjectName(QStringLiteral("customPlot"));
		gridLayout->addWidget(customPlot, 0, 0, 1, 4);

		startButton = new QPushButton(centralWidget);
		startButton->setObjectName(QStringLiteral("startButton"));
		gridLayout->addWidget(startButton, 2, 1, 1, 1);

		stopButton = new QPushButton(centralWidget);
		stopButton->setObjectName(QStringLiteral("stopButton"));
		gridLayout->addWidget(stopButton, 2, 2, 1, 1);

		returnButton = new QPushButton(centralWidget);
		returnButton->setObjectName(QStringLiteral("returnButton"));
		gridLayout->addWidget(returnButton, 2, 3, 1, 1);

		startButton->setText("Start");
		stopButton->setText("Stop");
		returnButton->setText("Back");

		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
		gridLayout->addItem(horizontalSpacer, 2, 0, 1, 1);

		centralWidgetLayout->addLayout(gridLayout, 0, 0, 1, 1);
		m_uMainWindow.get()->setCentralWidget(centralWidget);
	}

	void BaseScene::mf_CleanPlot()
	{
		customPlot->graph(0)->data()->clear();
		customPlot->replot();
	}

	void BaseScene::returnClick()
	{
		emit SceneChange("InitialScene");
	}

	void BaseScene::mf_PlotRandom()
	{
		readValues = validValues.split(",");
		if (readValues.length() < 3)
		{
			serialData = arduinoDevice->readAll();
			validValues += QString::fromStdString(serialData.toStdString());
		}
		else
		{
			for (int i = 0; i < yAxis.size(); i++)
				yAxis[i] = readValues[1].toInt() * sin(2 * M_PI * freq * xAxis[i]);
			customPlot->graph(0)->setData(xAxis, yAxis);
			customPlot->xAxis->setRange(0, 0.1);
			customPlot->yAxis->setRange(-750, 750);
			customPlot->replot();
			validValues = "";
		}
	}

	void BaseScene::mf_StartTimer()
	{
		if (arduinoAvailable)
		{
			arduinoDevice->open(QSerialPort::ReadOnly);
			connect(arduinoDevice, SIGNAL(readyRead()), this, SLOT(mf_PlotRandom()));
		}
	}

	void BaseScene::mf_StopTimer()
	{
		if (arduinoAvailable)
		{
			arduinoDevice->close();
			mf_CleanPlot();
		}
	}
}
