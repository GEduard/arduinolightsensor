#include <spring\Application\ApplicationModel.h>
#include <spring\Application\InitialScene.h>
#include <spring\Application\BaseScene.h>

const std::string initialSceneName = "InitialScene";
const std::string secondSceneName = "BaseScene";

namespace Spring
{
	ApplicationModel::ApplicationModel()
	{

	}

	void ApplicationModel::defineScene()
	{
		IScene* initialScene = new InitialScene(initialSceneName);
		IScene* secondScene = new BaseScene(secondSceneName);
		m_Scenes.emplace(initialSceneName, initialScene);
		m_Scenes.emplace(secondSceneName, secondScene);
	}

	void ApplicationModel::defineInitialScene()
	{
		mv_szInitialScene = initialSceneName;
	}

	void ApplicationModel::defineTransientData()
	{
		std::string appName = "";
		m_TransientData.emplace("appNameLEdit", appName);
		unsigned int sampleRate = 25600;
		m_TransientData.emplace("sampleRateDSBox", sampleRate);
		double displayTime = 0.10;
		m_TransientData.emplace("displayTimeDSBox", displayTime);
		unsigned int refreshRate = 1;
		m_TransientData.emplace("refreshRateDSBox", refreshRate);
	}
}
