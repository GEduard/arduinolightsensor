#pragma once

#include <iostream>
#include "qcustomplot.h"
#include <qpushbutton.h>
#include <qgridlayout.h>
#include <spring\Framework\IScene.h>
#include <QtSerialPort\qserialport.h>
#include <QtSerialPort\qserialportinfo.h>

namespace Spring
{
	class BaseScene : public IScene
	{
		Q_OBJECT
			private slots:
				void mf_StartTimer();
				void mf_StopTimer();
				void mf_PlotRandom();
				void mf_CleanPlot();
				void returnClick();

		private:
			QWidget *centralWidget;
			QGridLayout *centralWidgetLayout;
			QCustomPlot *customPlot;
			QGridLayout *gridLayout;
			QPushButton *stopButton;
			QPushButton *startButton;
			QPushButton *returnButton;
			QSpacerItem *horizontalSpacer;
			QVector<double> xAxis;
			QVector<double> yAxis;
			QSerialPort *arduinoDevice;
			QString portName;
			QByteArray serialData;
			QStringList readValues;
			QString validValues;
			double displayTime;
			unsigned int refreshRate;
			unsigned int sampleRate;
			bool arduinoAvailable;
			int freq;

		public:
			explicit BaseScene(const std::string &ac_szSceneName);

			void createScene()override;

			void release()override;

			void createGUI();
	};
}
