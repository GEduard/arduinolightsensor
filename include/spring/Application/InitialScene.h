#pragma once

#include <qlabel.h>
#include <qspinbox.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qgridlayout.h>
#include <spring\Framework\IScene.h>

namespace Spring
{
	class InitialScene : public IScene
	{
		Q_OBJECT
			private slots:
				void mf_OkButton();

		private:
			QWidget *centralWidget;
			QGridLayout *gridLayout;
			QGridLayout *centralWidgetLayout;
			QLabel *appNameLabel;
			QLabel *refreshRateLabel;
			QLabel *displayTimeLabel;
			QLabel *sampleRateLabel;
			QPushButton *okButton;
			QLineEdit *appNameLEdit;
			QDoubleSpinBox *sampleRateDSBox;
			QDoubleSpinBox *refreshRateDSBox;
			QDoubleSpinBox *displayTimeDSBox;
			QSpacerItem *verticalSpacer1;
			QSpacerItem *verticalSpacer2;
			QSpacerItem *horizontalSpacer1;
			QSpacerItem *horizontalSpacer2;

		public:
			explicit InitialScene(const std::string &ac_szSceneName);

			void createScene()override;

			void release()override;

			void createGUI();
	};
}
